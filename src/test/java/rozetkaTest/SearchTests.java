package rozetkaTest;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;
import page_elements.TopBar;
import java.util.List;


public class SearchTests extends BaseTest{


    @Test(alwaysRun = true)
    public void checkSearchLine(){
        driver.get("https://rozetka.com.ua/");

        TopBar searchLine = new TopBar(driver);
        List<WebElement>  productTitles = searchLine
                .fillSearchLine("macbook", Keys.ENTER)
                .waitSearchResults()
                .getProductTitles();

        Assert.assertTrue(productTitles.stream()
                .anyMatch(title -> title.getText().toLowerCase().contains("macbook")), "Product is found");
    }


    @Test(alwaysRun = true)
    public void checkSuggestionSearch(){
        driver.get("https://rozetka.com.ua/");

        TopBar searchLine = new TopBar(driver);
        List<WebElement>  productTitles = searchLine
                .fillSearchLine("iphone")
                .clickOnSuggestion("iphone 13")
                .waitSearchResults()
                .getProductTitles();

        Assert.assertTrue(productTitles.stream()
                .anyMatch(title -> title.getText().toLowerCase().contains("iphone 13")), "Product is found");

    }

}