package rozetkaTest;

import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;
import page_elements.Product;

public class CartTest extends BaseTest{

    @Test(alwaysRun = true)
    public void checkAddingToCart(){
        driver.get("https://rozetka.com.ua/xiaomi_redmi_9a_2_32gb_granite_gray_m2006c3lg/p232347079/");

        Product product = new Product(driver);
        WebElement cart = product.clickOnBuyButton()
                .getProductFromCart();

        Assert.assertTrue(cart.isDisplayed(), "Product added to the cart");
    }


    @Test(alwaysRun = true)
    public void checkRemovingFromCart(){
        driver.get("https://rozetka.com.ua/xiaomi_redmi_9a_2_32gb_granite_gray_m2006c3lg/p232347079/");

        Product product = new Product(driver);
        boolean cartIsEmpty = product.clickOnBuyButton()
                .openPopupMenu()
                .clickOnRemoveButton()
                .checkCartIsEmpty();

        Assert.assertTrue(cartIsEmpty, "Product deleted from the cart");
    }

}
