package rozetkaTest;

import org.openqa.selenium.Keys;
import org.testng.Assert;
import org.testng.annotations.Test;
import page_elements.Catalog;
import page_elements.TopBar;
import static org.assertj.core.api.Assertions.*;



public class NavigationTests extends BaseTest{
    @Test(alwaysRun = true)
    public void checkNextPage() {
        driver.get("https://rozetka.com.ua/");

        TopBar searchLine = new TopBar(driver);
        int activePage = searchLine
                .fillSearchLine("телевизор", Keys.ENTER)
                .clickOnPageNumber(2)
                .getActivePageNumber();

        Assert.assertEquals(activePage, 2, "Next page is opened");
    }

    @Test(alwaysRun = true)
    public void checkShowMoreProducts(){
        driver.get("https://rozetka.com.ua/");

        TopBar searchLine = new TopBar(driver);
        int activePage = searchLine
                .fillSearchLine("телевизор", Keys.ENTER)
                .clickOnShowMoreButton()
                .getActivePageNumber();

        Assert.assertEquals(activePage, 2, "Next page is opened");
    }


    @Test(alwaysRun = true)
    public void checkNavigationMenu() {
        driver.get("https://rozetka.com.ua/");

        Catalog catalog = new Catalog(driver);
        String categoryHeading = catalog.clickOnCategories()
                .moveToCategoryItem()
                .chooseCategorySubItem()
                .getCategoryHeading();

        System.out.println(categoryHeading);
        assertThat(categoryHeading).isIn("Кофемашины", "Кавомашини");
    }

}
