package rozetkaTest;

import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;
import page_elements.SearchResults;
import java.util.List;

public class FilterTests extends  BaseTest{

    @Test(alwaysRun = true)
    public void checkFilter(){
        driver.get("https://rozetka.com.ua/printers-mfu/c80007/");

        SearchResults results = new SearchResults(driver);
        List<WebElement> productTitles= results.waitSearchResults().
                clickOnCheckBoxFilter("Canon")
                .waitSearchResults()
                .getProductTitles();

        Assert.assertTrue(productTitles.stream()
                .anyMatch(title -> title.getText().toLowerCase().contains("canon")), "Products are filtered");
    }


    @Test(alwaysRun = true)
    public void checkSorting(){
        driver.get("https://rozetka.com.ua/ua/printers-mfu/c80007/seller=rozetka/");

        SearchResults results = new SearchResults(driver);
        WebElement productTitle = results.waitSearchResults()
                .sortFromExpensiveToCheap()
                .waitSearchResults()
                .getProductTitles().get(0);

        Assert.assertTrue(productTitle.getText().contains("Epson L15160 A3"), "Products were sorted");
    }

    @Test(alwaysRun = true)
    public void checkPriceFilter() {
        driver.get("https://bt.rozetka.com.ua/ua/washing_machines/c80124/price=9914-101523/");

        SearchResults results = new SearchResults(driver);
        List<WebElement> productPrices = results.waitSearchResults()
                .setMinPrice(30000)
                .waitSearchResults()
                .getProductPrices();

        Assert.assertTrue(productPrices.stream()
                .allMatch(price -> Integer.parseInt(price.getText().replace(" ", "")) > 30000), "Products were filtered");
    }

    @Test(alwaysRun = true)
    public void checkFilterSearchLine() {
        driver.get("https://rozetka.com.ua/ua/mobile-phones/c80003/");

        SearchResults results = new SearchResults(driver);
        int filteredItems = results.waitSearchResults()
                .sendKeyToFilterSearchLine("sam")
                .countFilteredItems();

        Assert.assertEquals(filteredItems,1, "Producer list changed");

    }



}
