package rozetkaTest;

import org.testng.Assert;
import org.testng.annotations.Test;
import page_elements.TopBar;

public class GeneralTests extends BaseTest{
    @Test(alwaysRun = true)
    public void checkLanguageChange(){
        driver.get("https://rozetka.com.ua/");

        TopBar langSection = new TopBar(driver);
        String activeLang = langSection.clickOnUALangButton()
                .getActiveLanguage();

        Assert.assertEquals(activeLang, "UA");
    }

}
