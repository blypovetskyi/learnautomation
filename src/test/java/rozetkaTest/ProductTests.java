package rozetkaTest;

import org.testng.Assert;
import org.testng.annotations.Test;
import page_elements.Product;
import page_elements.SearchResults;

public class ProductTests extends BaseTest {
    @Test(alwaysRun = true)
    public void checkFeatureTab(){
        driver.get("https://rozetka.com.ua/37575708/g37575708/");

        Product product = new Product(driver);

        boolean featureTab = product.openFeatureTab()
                .checkFeatureTabIsDisplayed();

        Assert.assertTrue(featureTab, "Feature tab is displayed");

    }


    @Test(alwaysRun = true)
    public void checkReviewsTab(){
        driver.get("https://rozetka.com.ua/37575708/g37575708/");

        Product product = new Product(driver);

        boolean reviewTab = product.openReviewTab()
                .checkReviewTabIsDisplayed();

        Assert.assertTrue(reviewTab, "Reviews tab is displayed");
    }


    @Test(alwaysRun = true)
    public void checkAddingToComparison(){
        driver.get("https://rozetka.com.ua/ua/tablets/c130309/producer=apple/");

        SearchResults searchResult = new SearchResults(driver);

        boolean comparisonButton = searchResult.clickOnProduct("Планшет Apple iPad Air 10.9\" Wi-Fi 64 GB Sky Blue (MYFQ2RK/A)")
                .comparisonButtonIsActive();

        Assert.assertTrue(comparisonButton, "Product added to comparison");
    }
}
