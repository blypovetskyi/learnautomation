import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;
import java.util.concurrent.TimeUnit;

public class SearchTest {

    @Test
    public void checkGoogleIsOpened(){
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.get("http://www.google.com");
        WebElement element = driver.findElement(By.xpath("//img[@alt='Google']"));
        boolean result = element.isDisplayed();
        System.out.println(result);
        Assert.assertTrue(result, "Google is opened");
        driver.quit();
        }

    @Test
    public void findTableOnRozetka(){
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.get("https://rozetka.com.ua/");
        WebElement element = driver.findElement(By.xpath("//input[@name='search']"));
        element.sendKeys("Офисный стол" + Keys.ENTER);

        WebElement elem =  new WebDriverWait(driver, 10)
                .until(ExpectedConditions.elementToBeClickable(By.xpath("//*[contains(text(), 'Офисный органайзер скрытая подставка')]")));


//       WebElement officeTable = driver.findElement(By.xpath("//*[contains(text(), 'Офисный органайзер скрытая подставка')]"));
        elem.click();

        driver.quit();
    }


    @Test
    public void openRozetka(){
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        try {
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            driver.get("https://rozetka.com.ua/");
            WebElement element = null;
            WebElement element2 = null;
            Actions actions = new Actions(driver);
            actions.click(element)
                    .doubleClick(element)
                    .build()
                    .perform();
            actions.clickAndHold(element)
                    .keyDown("c"+Keys.SHIFT)
                    .keyUp(Keys.SHIFT)
                    .moveToElement(element2)
                    .pause(2000)
                    .release(element2)
                    .build()
                    .perform();
        } finally {
            driver.quit();
        }
    }

}
