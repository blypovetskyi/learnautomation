package page_elements;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Product extends BasePage{
    String featureButtonPath = "//ul[@class='tabs__list']/li/a[contains(text(), 'Характеристики')]";
    String featureTabPath = "//*[@class='product-tabs__content']/h2[contains(text(), 'Характеристики')]";
    String reviewButtonPath = "//*[@class='product__rating']/a";
    String reviewTabPath = "//*[@class='product-comments__list']";
    String buyButtonPath = "//app-buy-button/button/span[contains(text(), 'Купить') or contains(text(), 'Купити')]";

    public Product(WebDriver driver){
        this.driver = driver;
    }

    public Product openFeatureTab(){
        WebElement featureButton = new WebDriverWait(driver, 10)
                .until(ExpectedConditions.elementToBeClickable(By.xpath(featureButtonPath)));
        featureButton.click();
        return this;
    }

    public boolean checkFeatureTabIsDisplayed(){
        WebElement featureTab = new WebDriverWait(driver, 10)
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath(featureTabPath)));
        return featureTab.isDisplayed();
    }

    public Product openReviewTab(){
        WebElement reviewsLink = new WebDriverWait(driver, 10)
                .until(ExpectedConditions.elementToBeClickable(By.xpath(reviewButtonPath)));
        reviewsLink.click();
        return this;
    }

    public boolean checkReviewTabIsDisplayed(){
        WebElement reviewsTab = new WebDriverWait(driver, 10)
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath(reviewTabPath)));
        return  reviewsTab.isDisplayed();
    }

    public Cart clickOnBuyButton(){
        WebElement buyButton = new WebDriverWait(driver, 10)
                .until(ExpectedConditions.elementToBeClickable(By.xpath(buyButtonPath)));
        buyButton.click();
        return new Cart(driver);
    }
}
