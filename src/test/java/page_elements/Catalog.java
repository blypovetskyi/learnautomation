package page_elements;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Catalog extends BasePage{
    String categoryButtonPath = "//*[@id='fat-menu']";
    String appliancesItemPath = "//a[@class='menu-categories__link' and (contains(text(), 'Бытовая техника') or contains(text(), 'Побутова техніка'))]";
    String appliancesSubItemPath = "//a[@class='menu__link' and (contains(text(), 'Кофеварки') or contains(text(), 'Кавоварки'))]";

    public Catalog(WebDriver driver){
        this.driver = driver;
    }

    public Catalog clickOnCategories(){
        WebElement categoriesMenu = new WebDriverWait(driver, 10)
                .until(ExpectedConditions.elementToBeClickable(By.xpath(categoryButtonPath)));

        Actions actions = new Actions(driver);
        actions.click(categoriesMenu)
                .build()
                .perform();
        return this;
    }

    public Catalog moveToCategoryItem(){
        WebElement menuItem = driver.findElement(By.xpath(appliancesItemPath));
        Actions actions = new Actions(driver);
        actions.moveToElement(menuItem)
                .pause(2000)
                .build()
                .perform();
        return new Catalog(driver);
    }


    public SearchResults chooseCategorySubItem(){
        WebElement menuSubItem = driver.findElement(By.xpath(appliancesSubItemPath));
        Actions actions = new Actions(driver);
        actions.moveToElement(menuSubItem)
                .pause(1000)
                .click(menuSubItem)
                .build()
                .perform();
        return new SearchResults(driver);
    }
}
