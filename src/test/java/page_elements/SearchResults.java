package page_elements;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.List;

public class SearchResults extends BasePage{
    String productTitlePath = "//span[@class='goods-tile__title']";
    String showMoreButtonPath = "//*[@class='show-more show-more--horizontal']";
    String activePaginationItemPath = "//a[@class='pagination__link ng-star-inserted pagination__link--active']";
    String headerPath = "//h1[@class='catalog-heading ng-star-inserted']";
    String comparisonButtonPath = "//rz-comparison/button";
    String sortingDropdownPath = "//*[@class='catalog-settings__sorting']/select";
    String activeLangPath = "//*[@class='lang__link lang__link--active ng-star-inserted']";
    String minPriceInputPath = "//rz-filter-slider//input[1]";
    String productPricePath = "//span[@class='goods-tile__price-value']";
    String producerFilterPath = "//div[@data-filter-name='producer']//input[@type='search']";
    String producersPath = "//div[@data-filter-name='producer']//li";


    public SearchResults(WebDriver driver){
        this.driver = driver;
    }

    public List<WebElement> getProductTitles(){
    List<WebElement> productTitles = new WebDriverWait(driver, 20)
            .until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(productTitlePath)));
    return productTitles;
    }

    public SearchResults clickOnPageNumber(int pageNumber){
        WebElement paginationItem = new WebDriverWait(driver, 10)
                .until(ExpectedConditions.elementToBeClickable(By.xpath("//a[@class='pagination__link ng-star-inserted' and contains(text(), '"+pageNumber +"')]")));
        paginationItem.click();
        return this;
    }

    public SearchResults clickOnShowMoreButton(){
        WebElement showMoreButton = new WebDriverWait(driver, 10)
                .until(ExpectedConditions.elementToBeClickable(By.xpath(showMoreButtonPath)));
        showMoreButton.click();
        return this;
    }

    public int getActivePageNumber(){
        WebElement activePaginationItem = new WebDriverWait(driver, 10)
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath(activePaginationItemPath)));
        return Integer.parseInt(activePaginationItem.getText());
    }

    public String getCategoryHeading (){
        WebElement catalogHeading = new WebDriverWait(driver, 20)
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath(headerPath)));
        return catalogHeading.getText().trim();
    }

    public SearchResults clickOnProduct(String productName){
        WebElement item = new WebDriverWait(driver, 10)
                .until(ExpectedConditions.elementToBeClickable(By.xpath("//span[@class='goods-tile__title' and" +
                        " contains(text(), '" + productName+"')]/../..//button[@class='compare-button ng-star-inserted']")));
        item.click();
        return this;
    }

    public boolean comparisonButtonIsActive(){
        WebElement comparisonButton = new WebDriverWait(driver, 10)
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath(comparisonButtonPath)));
        return comparisonButton.isDisplayed();
    }

    public SearchResults clickOnCheckBoxFilter(String filterItem){
        WebElement checkbox = new WebDriverWait(driver, 10)
                .until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@data-id='"+ filterItem + "']")));
        checkbox.click();
        return this;
    }

    public SearchResults waitSearchResults(){
        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.visibilityOfElementLocated(By.xpath(productTitlePath)));
        return this;
    }


    public SearchResults sortFromExpensiveToCheap(){
        WebElement element = new WebDriverWait(driver, 10)
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath(sortingDropdownPath)));
        Select dropdown = new Select(element);

        WebElement lang = new WebDriverWait(driver, 10)
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath(activeLangPath)));

        if (lang.getText().contains("RU"))
            dropdown.selectByVisibleText("От дорогих к дешевым");
        else
            dropdown.selectByVisibleText("Від дорогих до дешевих");
        return this;
    }

    public SearchResults setMinPrice(int price){
        WebElement minPrice = new WebDriverWait(driver, 10)
                .until(ExpectedConditions.elementToBeClickable(By.xpath(minPriceInputPath)));

        minPrice.clear();
        Actions actions = new Actions(driver);
        actions.click(minPrice)
                .sendKeys(String.valueOf(price) + Keys.ENTER)
                .pause(5000)
                .build()
                .perform();
        return this;
    }

    public List<WebElement> getProductPrices(){
        List<WebElement> prices = new WebDriverWait(driver, 10)
                .until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(productPricePath)));
        return prices;
    }

    public SearchResults sendKeyToFilterSearchLine(String key){
        WebElement searchLine = new WebDriverWait(driver, 10)
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath(producerFilterPath)));

        Actions actions = new Actions(driver);
        actions.click(searchLine)
                .sendKeys(key)
                .pause(3000)
                .build()
                .perform();
        return this;
    }

    public int countFilteredItems(){
        List<WebElement> producers = driver.findElements(By.xpath(producersPath));
        return producers.size();
    }
}
