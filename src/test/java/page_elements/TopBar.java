package page_elements;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TopBar extends BasePage{
    String searchLinePath = "//input[@name='search']";
    String langButtonPath = "//rz-lang/ul/li/a[contains(text(), 'UA')]";
    String activeLangPath = "//li[@class='lang__item lang-header__item lang-header__item_state_active ng-star-inserted']/span";


    public TopBar(WebDriver driver){
        this.driver = driver;
    }

    public TopBar fillSearchLine(String searchText){
        WebElement searchLine = new WebDriverWait(driver, 10)
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath(searchLinePath)));
        searchLine.sendKeys(searchText);
        return this;
    }

    public SearchResults fillSearchLine(String searchText, Keys key){
        WebElement searchLine = new WebDriverWait(driver, 10)
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath(searchLinePath)));
        searchLine.sendKeys(searchText +  key);
        return new SearchResults(driver);
    }


    public SearchResults clickOnSuggestion(String suggestion){
        WebElement searchSuggestion = new WebDriverWait(driver, 10)
                .until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@data-name='"+suggestion+"']")));
        searchSuggestion.click();
        return new SearchResults(driver);
    }

    public TopBar clickOnUALangButton(){
        WebElement langButton = new WebDriverWait(driver, 10)
                .until(ExpectedConditions.elementToBeClickable(By.xpath(langButtonPath)));
        langButton.click();
        return this;
    }

    public String getActiveLanguage(){
        WebElement activeLang = new WebDriverWait(driver, 10)
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath(activeLangPath)));
        return activeLang.getText().trim();
    }

}

