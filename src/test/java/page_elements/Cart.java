package page_elements;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Cart extends BasePage{
    String cartItemPath = "//ul[@class='cart-list ng-star-inserted']";
    String contextMenuPath = "//rz-popup-menu/button";
    String removeButtonPath = "//rz-trash-icon/button";
    String emptyCartPath = "//div[@data-testid='empty-cart']";

    public Cart(WebDriver driver){
        this.driver = driver;
    }

    public WebElement getProductFromCart(){
        WebElement productInCart = new WebDriverWait(driver, 10)
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath(cartItemPath)));
        return productInCart;
    }

    public Cart openPopupMenu(){
        WebElement contextMenu = new WebDriverWait(driver, 10)
                .until(ExpectedConditions.elementToBeClickable(By.xpath(contextMenuPath)));
        contextMenu.click();
        return this;
    }

    public Cart clickOnRemoveButton(){
        WebElement deleteButton = new WebDriverWait(driver, 10)
                .until(ExpectedConditions.elementToBeClickable(By.xpath(removeButtonPath)));
        deleteButton.click();
        return this;
    }

    public boolean checkCartIsEmpty(){
        WebElement productInCart = new WebDriverWait(driver, 10)
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath(emptyCartPath)));
        return productInCart.isDisplayed();
    }
}
